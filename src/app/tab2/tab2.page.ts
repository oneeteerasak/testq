import { Component } from '@angular/core';
import { BannerService } from '../services/banner.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  private bannerList: Banners[];
  private CategoryList: Books[];
  private DatumList: Data[];
  private data: Data[];
  private newsData: any;
  private label: any[] = [];
  private bookList: Books[];

  constructor(private bannerService: BannerService) {
    let dataValues = [];
    let dataKeys = [];
    this.bannerService.getBannerList().subscribe((Response) => {
      this.bannerList = Response;
    });

    this.bannerService.getCategoryList().subscribe((Response) => {
      this.DatumList = Response;
      console.log(this.DatumList);
    });

    this.bannerService.getCategoryList().subscribe(deta => {
      this.data = deta;
    });
  }
}

interface Banners {
  type: string;
  param: string;
  imgurl: string;
}

interface Books {
  Id: number;
  Name: string;
  Author: string;
  Img: string;
  Desc: string;
  CName: string;
  Score: string;
}

interface Data {
  Category: string;
  NavFlag: string;
  Books: Books[];
}


