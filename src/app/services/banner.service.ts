import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private http: Http) { }

  getBannerList(){
    return this.http.get("https://api.webnovelsapp.com/v5/base/banner_man.html").map((res)=>res.json().data);
  }

  getCategoryList(){
    return this.http.get("https://api.webnovelsapp.com/v5/base/man.html").map((res)=>res.json().data);
  }

}
