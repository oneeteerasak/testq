import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
var BannerService = /** @class */ (function () {
    function BannerService(http) {
        this.http = http;
    }
    BannerService.prototype.getBannerList = function () {
        return this.http.get("https://api.webnovelsapp.com/v5/base/banner_man.html").map(function (res) { return res.json().data; });
    };
    BannerService.prototype.getCategoryList = function () {
        return this.http.get("https://api.webnovelsapp.com/v5/base/man.html").map(function (res) { return res.json().data; });
    };
    BannerService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Http])
    ], BannerService);
    return BannerService;
}());
export { BannerService };
//# sourceMappingURL=banner.service.js.map